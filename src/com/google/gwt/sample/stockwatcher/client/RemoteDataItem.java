package com.google.gwt.sample.stockwatcher.client;

import com.google.gwt.user.client.rpc.IsSerializable;

public class RemoteDataItem implements IsSerializable {
    private String name;
    private int age;
    private String gender;
    private String city;

    // A no-argument constructor is necessary for GWT serialization
    public RemoteDataItem() {
        // Initialize fields with default values if necessary
    }

    // Constructor with parameters for ease of creation
    public RemoteDataItem(String name, int age, String gender, String city) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.city = city;
    }

    // Getters
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getCity() {
        return city;
    }

    // Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
