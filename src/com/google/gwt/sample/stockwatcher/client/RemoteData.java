package com.google.gwt.sample.stockwatcher.client;

import com.google.gwt.user.client.rpc.GwtTransient;
import com.google.gwt.user.client.rpc.IsSerializable;

public class RemoteData implements IsSerializable {

    @GwtTransient  // This tells GWT not to obfuscate this field
    String test = "str test";
    @GwtTransient  // This tells GWT not to obfuscate this field
    RemoteDataItem[] items;

    public RemoteData() {
        this.items = new RemoteDataItem[2];  // Deciding the size of the array
        this.items[0] = new RemoteDataItem("Alice", 30, "Female", "New York");
        this.items[1] = new RemoteDataItem("Bob", 25, "Male", "Los Angeles");
    }

    public RemoteData(String name) {
        this.items = new RemoteDataItem[5];  // Deciding the size of the array
        this.items[0] = new RemoteDataItem(name, 30, "Female", "New York");
        this.items[1] = new RemoteDataItem(name, 25, "Male", "Los Angeles");
        this.items[2] = new RemoteDataItem(name, 28, "Female", "Munich");
        this.items[3] = new RemoteDataItem(name, 41, "Male", "Amsterdam");
        this.items[4] = new RemoteDataItem(name, 19, "Male", "JiangXi");
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public RemoteDataItem[] getItems() {
        return items;
    }

    public void setItems(RemoteDataItem[] items) {
        this.items = items;
    }
}
