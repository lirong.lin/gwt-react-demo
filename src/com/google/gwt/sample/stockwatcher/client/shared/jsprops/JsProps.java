package com.google.gwt.sample.stockwatcher.client.shared.jsprops;

import com.google.gwt.sample.stockwatcher.client.shared.jsinterface.JsBiConsumer;
import com.google.gwt.sample.stockwatcher.client.shared.jsinterface.JsConsumer;
import com.google.gwt.sample.stockwatcher.client.shared.jsinterface.JsRunnable;
import com.google.gwt.sample.stockwatcher.client.shared.jsinterface.JsTriConsumer;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public interface JsProps {

    @JsProperty
    void setComponent(String component);

    @JsProperty
    void setTitle(String title);

    @JsProperty
    void setLoading(boolean loading);

    @JsProperty
    void setData(Object data);

    @JsProperty
    void setContent(String content);

    @JsProperty
    void setMessage(String message);

    @JsProperty
    void setOnClick(JsRunnable callback);

    @JsProperty
    void setOnClick(JsConsumer callback);

    @JsProperty
    void setOnClick(JsBiConsumer callback);

    @JsProperty
    void setOnClick(JsTriConsumer callback);
}
