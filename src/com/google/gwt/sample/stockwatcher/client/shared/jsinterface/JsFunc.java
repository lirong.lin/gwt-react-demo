package com.google.gwt.sample.stockwatcher.client.shared.jsinterface;

import jsinterop.annotations.JsFunction;

import java.util.function.Function;
import java.util.function.Supplier;

// takes 1 param and return value
@JsFunction
public interface JsFunc {
    Object apply(Object param1);
}
