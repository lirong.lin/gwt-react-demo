package com.google.gwt.sample.stockwatcher.client.shared.jsconnect;

import com.google.gwt.sample.stockwatcher.client.shared.jsprops.JsProps;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

/**
 * this class is mount to the ComponentManager in react project
 */
@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "ComponentManager")
public class ComponentManager {

    /**
     * render react component {currentComponentName} on {parentId} html element
     * @param parentId the html id of the parent of the component you want react to render
     * @param currentComponentName Specify the component name you want React to render.
     * @param props props pass to the target component
     */
    public static native void render(String parentId, String currentComponentName, JsProps props);

    /**
     * unmount react component of {parentId} html element
     * @param parentId the html id of the parent of the component you want react to unmount
     */
    public static native void unmount(String parentId);

}
