package com.google.gwt.sample.stockwatcher.client.shared.jsinterface;

import jsinterop.annotations.JsFunction;

// takes 0 param and return value
@JsFunction
public interface JsSupplier {
    Object get();
}
