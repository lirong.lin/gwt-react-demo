package com.google.gwt.sample.stockwatcher.client.shared.jsinterface;

import jsinterop.annotations.JsFunction;


@JsFunction
public interface JsBiConsumer {
    void accept(Object param1, Object param2);
}
