package com.google.gwt.sample.stockwatcher.client.shared.jsinterface;

import jsinterop.annotations.JsFunction;

import java.util.function.BiFunction;
import java.util.function.Function;

// takes 2 params and return value
@JsFunction
public interface JsBiFunc  {
    Object apply(Object param1, Object param2);
}
