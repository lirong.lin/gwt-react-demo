package com.google.gwt.sample.stockwatcher.client.shared.jsinterface;

import jsinterop.annotations.JsFunction;

// takes 3 param and return void
@JsFunction
public interface JsTriConsumer {
    void accept(Object param1, Object param2, Object param3);
}
