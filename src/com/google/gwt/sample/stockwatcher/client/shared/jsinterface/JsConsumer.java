package com.google.gwt.sample.stockwatcher.client.shared.jsinterface;

import jsinterop.annotations.JsFunction;

import java.util.function.Consumer;

// takes 1 param and return void
@JsFunction
public interface JsConsumer {
    void accept(Object param1);
}
