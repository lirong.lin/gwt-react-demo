package com.google.gwt.sample.stockwatcher.client.shared.jsprops;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsProperty;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public interface ReactSearchProps extends JsProps {

    @JsProperty
    void setSearchText(String searchText);
}
