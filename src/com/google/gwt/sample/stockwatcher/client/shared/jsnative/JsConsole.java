package com.google.gwt.sample.stockwatcher.client.shared.jsnative;


import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "console")
public class JsConsole {
    public static native void log(Object... objs);
    public static native void error(Object... objs);
    public static native void warn(Object... objs);
}