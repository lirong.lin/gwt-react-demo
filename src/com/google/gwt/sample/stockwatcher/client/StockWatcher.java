package com.google.gwt.sample.stockwatcher.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.sample.stockwatcher.client.shared.jsconnect.ComponentManager;
import com.google.gwt.sample.stockwatcher.client.shared.jsprops.ReactSearchProps;
import com.google.gwt.sample.stockwatcher.client.shared.jsnative.JsConsole;
import com.google.gwt.sample.stockwatcher.client.shared.jsprops.JsProps;
import com.google.gwt.user.client.rpc.AsyncCallback;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

public class StockWatcher implements EntryPoint {

    /**
     * Create a remote service proxy to talk to the server-side Greeting service.
     */
    private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);


    /**
     * Entry point method.
     */
    public void onModuleLoad() {

        ReactSearchProps props = Js.uncheckedCast(JsPropertyMap.of());
        props.setLoading(true);
        props.setOnClick((searchText) -> {
            props.setLoading(true);
            props.setSearchText(searchText.toString());
            ComponentManager.render("reactSearch", "SearchComponent", props);
            this.loadDataFromServer(searchText.toString(), props);
        });
        ComponentManager.render("reactSearch", "SearchComponent", props);
        this.loadDataFromServer("", props);

        //render sideBar component
        ComponentManager.render("sideBar", "SideBar", null);

        //render whole react app
        ComponentManager.render("app", "App", null);
    }

    private void loadDataFromServer(String searchText, JsProps props) {

        greetingService.greetServer(searchText, new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
                JsConsole.log("failed to get data, error = ", caught);
                props.setLoading(false);
                ComponentManager.render("reactSearch", "SearchComponent", props);
            }

            public void onSuccess(String remoteData) {
                props.setLoading(false);
                props.setData(remoteData);
                ComponentManager.render("reactSearch", "SearchComponent", props);
            }
        });
    }
}