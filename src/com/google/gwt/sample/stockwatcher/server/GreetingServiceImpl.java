package com.google.gwt.sample.stockwatcher.server;

import com.google.gwt.sample.stockwatcher.client.GreetingService;
import com.google.gwt.sample.stockwatcher.client.RemoteData;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.google.gson.Gson;
/**
 * The server-side implementation of the RPC service.
 */
public class GreetingServiceImpl extends RemoteServiceServlet implements
    GreetingService {

  public String greetServer(String input) throws IllegalArgumentException, InterruptedException {

    Thread.sleep(3000);
    Gson gson = new Gson();
    // Convert remoteData to JSON string

    return gson.toJson((input == null || input.isEmpty())?new RemoteData() : new RemoteData(input));
  }
}
