import './SearchComponent.css'
import React, {useState} from "react";
import {
    Button,
    CircularProgress,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField
} from "@mui/material";


function SearchComponent({loading, searchText, data, onClick}) {

    // State to hold the input value
    const [inputValue, setInputValue] = useState(searchText ? searchText : "");

    // Function to update state based on input changes
    const handleInputChange = (event) => {
        setInputValue(event.target.value);
    };

    return (<div>
        <h1>React Search Component </h1>
        <div className="search-field">
            <TextField className="search-input" value={inputValue} onChange={handleInputChange} focused/>
            <Button variant="contained" size="large"
                    onClick={() => onClick(inputValue)}>
                Search Friends
            </Button>
        </div>
        <div className="search-table">
            {loading ? <CircularProgress className="loading-spinner"/> : data && (<TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>name</TableCell>
                            <TableCell>age</TableCell>
                            <TableCell>gender</TableCell>
                            <TableCell>city</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {JSON.parse(data)?.items.map((row, index) => (<TableRow key={index}>
                            <TableCell component="th" scope="row">
                                {row.name}
                            </TableCell>
                            <TableCell>{row.age}</TableCell>
                            <TableCell>{row.gender}</TableCell>
                            <TableCell>{row.city}</TableCell>
                        </TableRow>))}
                    </TableBody>
                </Table>
            </TableContainer>)}
        </div>
    </div>);
}

export default SearchComponent;