import SearchComponent from "./search-component/SearchComponent";
import SideBar from "./side-bar/SideBar";
import App from "./App";

// This map stores component-name mappings for dynamic components used in a GWT project.
export const componentNameMap: any = {
    'SearchComponent': SearchComponent,
    'SideBar': SideBar,
    'App': App
}