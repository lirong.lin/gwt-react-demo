import './index.css';
import ReactDOM from "react-dom/client";
import React from "react";
import {componentNameMap} from "./components";

window.ComponentManager = {
    roots: {},  // store root references to reuse
    render: (parentId, currentComponentName, props) => {
        console.log("props = ", props)
        console.log("parentId = ", parentId)
        console.log("currentComponentName = ", currentComponentName)
        const parentElement = document.getElementById(parentId);
        const roots = window.ComponentManager.roots;
        // Check if a root already exists. If not, create one to avoid unnecessary recreation and prevent memory leaks.
        if (!roots[parentId]) {
            roots[parentId] = ReactDOM.createRoot(parentElement);
        }

        const DynamicComponent = componentNameMap[currentComponentName];
        // reuse root so no need create a new one
        roots[parentId].render(
            <DynamicComponent {...props}/>
        );
    },
    unmount: (containerId) => {
        const roots = window.ComponentManager.roots;
        if (roots[containerId]) {
           roots[containerId].unmount();
            delete roots[containerId];
        }
    }
};

