import * as React from 'react';
import Box from '@mui/joy/Box';
import Drawer from '@mui/joy/Drawer';
import Button from '@mui/joy/Button';
import List from '@mui/joy/List';
import Divider from '@mui/joy/Divider';
import ListItem from '@mui/joy/ListItem';
import ListItemButton from '@mui/joy/ListItemButton';

export default function SideBar() {
    const [open, setOpen] = React.useState(false);

    const toggleSideBar =
        (inOpen: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
            if ( event.type === 'keydown') {
                return;
            }

            setOpen(inOpen);
        };

    return (
        <Box sx={{ display: 'flex', justifyContent: 'end', marginRight: '10px' }}>
            <Button variant="outlined" color="neutral" onClick={toggleSideBar(true)}>
                Open sidebar
            </Button>
            <Drawer open={open} onClose={toggleSideBar(false)}>
                <Box
                    role="presentation"
                    onClick={toggleSideBar(false)}
                    onKeyDown={toggleSideBar(false)}
                >
                    <List>
                        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text) => (
                            <ListItem key={text}>
                                <ListItemButton>{text}</ListItemButton>
                            </ListItem>
                        ))}
                    </List>
                    <Divider />
                    <List>
                        {['All mail', 'Trash', 'Spam'].map((text) => (
                            <ListItem key={text}>
                                <ListItemButton>{text}</ListItemButton>
                            </ListItem>
                        ))}
                    </List>
                </Box>
            </Drawer>
        </Box>
    );
}
